var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    Bicicleta.find({},function(err,bicicletas){
        res.status(200).json({
            bicicletas
        });
    });
    
}


exports.bicicleta_create = function(req, res){
    var bicicleta  = new Bicicleta({color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    bicicleta.save(function(err){
        res.status(200).json(bicicleta);
    });
};

exports.bicicleta_delete = function(req,res){
    Bicicleta.deleteOne({_id: req.body.id},function(err){});
    res.status(204).send();
}