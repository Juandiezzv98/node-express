

var mymap = L.map('mapid').setView([-12.126735, -77.011692], 15);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {

    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',

}).addTo(mymap);

//     L.marker([-12.126735, -77.011692]).addTo(mymap);
//     L.marker([-12.126735, -77.011692],{title:'Bicicleta 1'}).addTo(mymap);
//     L.marker([-10.126735, -67.011692]).addTo(mymap);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: 'ID: '+ bici.id}).addTo(mymap);
        })
    }
})