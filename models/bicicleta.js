var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function( color, modelo, ubicacion){  
    return new this({
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });

};

// bicicletaSchema.methods.toString = function(){
//     return 'code: '+ this.code + ' | ' + this.color 
// };  

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

// bicicletaSchema.statics.findByCode = function(aCode, cb){
//     return this.findOne({code: aCode}, cb);
// };

// bicicletaSchema.statics.removeByCode = function(aCode, cb){
//     return this.deleteOne({code: aCode}, cb);
// };

module.exports = mongoose.model('Bicicleta', bicicletaSchema);




// var Bicicleta = function(id, color, modelo,ubicacion){
//     this.id = id;
//     this.color = color ;
//     this.modelo = modelo; 
//     this.ubicacion = ubicacion;
// };
// module.exports = Bicicleta; 
// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//     Bicicleta.allBicis.push(aBici);
// }
// Bicicleta.findById = function(aBiciId){
//     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//     if(aBici)
//         return aBici;
//         else
//             throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
// }
// Bicicleta.removeById = function(aBiciId){
//    //Primero hacemos la busqueda y nos sercioramos que existe con el metodo findById
//    //Aunque mejor no :V 
//    // Bicicleta.findById(aBiciId);
//     for(var i = 0; i < Bicicleta.allBicis.length; i++){
//         if(Bicicleta.allBicis[i].id == aBiciId){
//             Bicicleta.allBicis.splice(i,1);
//             break;
//         }
//     }
// }

//var a =  this.createInstance(1,'rojo','urbana',[-34.6012424,-58.3861497]);
//var b =  this.createInstance(2,'marron','futurista',[-34.596932,-58.3808287]);
// // var a = new Bicicleta(1,'rojo','urbana',[-12.126735, -77.011692]);
// // var b = new Bicicleta(2,'marron','futurista',[-10.126735, -67.011692]);
//Bicicleta.add(a);
//Bicicleta.add(b);
